/**
 * *****
 * File: aula36.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Friday, 18 March 2022 10:54:43
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 18 March 2022 10:54:45
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 36. Herança, Polimorfismo e reutilização de código
 * *****
 */

class Forma {
    desenhar() {
        console.log('Eu sou uma forma');
    }
}

class Triangulo extends Forma {}

class Circulo extends Forma {
    desenhar() {
        console.log('Eu sou um circulo');
    }
}

const formas = [new Forma(), new Triangulo(), new Circulo()];
formas.forEach((forma) => forma.desenhar());

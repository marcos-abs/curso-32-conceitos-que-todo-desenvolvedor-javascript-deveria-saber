/**
 * *****
 * File: aula31.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 15:57:34
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 15:57:37
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 31. Async/Await
 * *****
 */
// const mamaeLembrou = true;
const mamaeLembrou = false;

const passarParaComprarBrinquedo = new Promise((resolve, reject) => {
    if (mamaeLembrou) {
        resolve(true);
    } else {
        reject(false);
    }
});

const sairParaBrincar = (resultado) => {
    return new Promise((resolve) => {
        if (resultado) {
            resolve('vou sair para brincar.');
        } else {
            resolve('não sairei para brincar');
        }
    });
};

passarParaComprarBrinquedo
    .then(sairParaBrincar)
    .catch(sairParaBrincar)
    .then((resultado) => console.log(resultado));

async function vamosParaALoja() {
    try {
        const mamaeLembrou = await passarParaComprarBrinquedo;
        const mensagem = await sairParaBrincar(mamaeLembrou);
        console.log(mensagem);
    } catch (naoLembrou) {
        const mensagem = await sairParaBrincar(naoLembrou);
        console.log(mensagem);
    }
}

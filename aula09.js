/**
 * *****
 * File: aula09.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Friday, 04 March 2022 15:15:55
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 05 March 2022 11:06:59
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				9. IIFE (Immediately Invocad Function Expression) e Namespaces
 *
 * fonte: https://stackabuse.com/sending-notifications-with-node-notifier-mac-windows-linux/
 * *****
 */

// const notifier = require('node-notifier');

// function alerta() {
//     notifier.notify('Olá Mundo!'); // alert('Olá Mundo!') não funciona.
// }

// alerta();

// !function () { // não funciona.
//     notifier.notify('Olá Mundo!');
// };

var dados = (function () {
    var contador = 0;
    return {
        incrementar: function () {
            contador++;
            return contador;
        },
    };
})(); //observar a posição e quantidade de parenteses

console.log(dados.incrementar());
console.log(dados.incrementar());
console.log(dados.incrementar());

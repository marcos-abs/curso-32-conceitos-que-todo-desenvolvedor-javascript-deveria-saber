/**
 * *****
 * File: aula37.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Friday, 18 March 2022 11:10:12
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 18 March 2022 11:10:14
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 37. Design Patterns: Module e Prototype
 * *****
 */
const Carro = {
    tracao: '4x4',
    ligar() {
        return 'ligou';
    },
};
const meuCarro = Object.create(Carro, { dono: { value: 'Gabriel' } });
// jshint -W103
Carro.__proto__.desligar = function () {
    return 'desligou';
};
// jshint +W103

console.log(meuCarro.ligar());
console.log(meuCarro.desligar());

/**
 * *****
 * File: aula06.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Thursday, 03 March 2022 17:28:06
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 03 March 2022 17:30:47
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				6. "==" vs "===" vs "typeof"
 * *****
 */

console.log(3 == '3'); // true

// 1° se ambos são do mesmo tipo
// 2° verifica se null == undefined, se for, ele retorna "true"
// 3° verifica se number == string, se for, ele converte a string em numero,
// 4° verifica se boolean == number, se for, ele converte o boolean num número,
// 5° verifica se boolean == string, se for, ele converte a string num boolean,
// 6° verifica se objeto == primitivo, se for, ele converte o objeto numa string.
console.log(3 === '3'); // false
console.log(3 === 3); // true

// É necessário que os valores e os tipos sejam iguais para ser true.

// typeof
// é ótimo para validação de tipos

console.log(typeof 'Gabriel' === 'string');

/**
 * *****
 * File: aula27.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 08:34:04
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 08:34:06
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 27. Recursion
 * *****
 */
function contagem(numero) {
    console.log('numero: ', numero);
    if (numero > 0) {
        contagem(numero - 1);
    }
}

contagem(5);

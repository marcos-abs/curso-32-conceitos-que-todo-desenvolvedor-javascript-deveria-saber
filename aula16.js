/**
 * *****
 * File: aula16.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Tuesday, 08 March 2022 15:20:37
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 08 March 2022 15:46:57
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				16. Classes
 * *****
 */
class Animal {
    constructor(pes) {
        this.pes = pes;
    }

    quantidadePes() {
        console.log('Este animal tem ' + this.pes + ' pés ou patas.');
    }
}

class Mamifero extends Animal {
    constructor(nome, som, pes) {
        super(pes);
        this.nome = nome;
        this.som = som;
    }

    emitirSom() {
        console.log('Este animal faz ' + this.som);
    }
}

const cachorro = new Mamifero('cachorro', 'auau', 4);

cachorro.emitirSom();
cachorro.quantidadePes();

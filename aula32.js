/**
 * *****
 * File: aula32.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 16:11:09
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 16:11:11
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 32. Data Structures: Stack e Queue
 * *****
 */

/** Exemplo1 de pilha */
// const pilha = [];
// pilha.unshift(0);
// pilha.unshift(1);
// console.log('pilha: ', pilha);
// pilha.unshift(2);
// pilha.unshift(3);
// pilha.shift();
// console.log('pilha: ', pilha);

/** Exemplo2 de pilha */
const pilha = [];
pilha.unshift(0);
pilha.unshift(1);
console.log('pilha: ', pilha);
pilha.unshift(2);
pilha.unshift(3);
pilha.shift();
console.log('pilha: ', pilha);

/** Exemplo1 de fila */
const fila = [];
fila.push(0);
fila.push(1);
console.log('fila: ', fila);
fila.push(2);
fila.push(3);
fila.shift();
console.log('fila: ', fila);

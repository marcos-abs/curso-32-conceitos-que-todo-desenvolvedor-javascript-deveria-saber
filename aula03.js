/**
 * *****
 * File: aula03.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Wednesday, 02 March 2022 10:46:56
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 March 2022 11:18:54
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				3. Tipos primitivos
 * *****
 */
// console.log(typeof true); // Boolean
// console.log(typeof Boolean(true)); // Boolean
// // jshint -W053
// console.log(typeof new Boolean(true)); // Object
// console.log(typeof new Boolean(true).valueOf()); // Boolean
// // jshint +W053
// console.log(typeof 'Gabriel'); // string
// console.log(typeof 28); // number
// console.log('Gabriel'.length); // number
// console.log(typeof 'Gabriel'.length); // number

// jshint -W053
var doze = new Number(12);
// jshint +W053
var quinze = doze + 3;
console.log(`quinze = ${quinze}`);
console.log(typeof doze);
console.log(typeof quinze);

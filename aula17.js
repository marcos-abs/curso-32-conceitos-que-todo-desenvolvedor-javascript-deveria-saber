/**
 * *****
 * File: aula17.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Tuesday, 08 March 2022 17:16:21
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 08 March 2022 17:16:22
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 *              17. This, call, apply e bind
 * *****
 */

// const dados = { nome: 'Gabriel' };
// const saudacao = function (idade) {
//     console.log(`Bem vindo ${this.nome}, sua idade é ${idade}`);
// };
// saudacao.call(dados, 28);

// const dados = { nome: 'Gabriel' };
// const argumentos = [28];
// const saudacao = function (idade) {
//     console.log(`Bem vindo ${this.nome}, sua idade é ${idade}`);
// };
// saudacao.apply(dados, argumentos);

const dados = { nome: 'Gabriel' };
const saudacao = function (idade) {
    console.log(`Bem vindo ${this.nome}, sua idade é ${idade}`);
};
const bound = saudacao.bind(dados);
bound(28);

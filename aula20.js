/**
 * *****
 * File: aula20.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Tuesday, 08 March 2022 17:45:00
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 09 March 2022 10:28:39
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 *              20. Object.create
 * *****
 */

// let Usuario = function (nome, idade) {
//     this.nome = nome;
//     this.idade = idade;
// };

// const gabriel = new Usuario('gabriel', 28);
// const novoGabriel = Object.create(gabriel);
// console.log(novoGabriel instanceof Usuario);
// console.log(novoGabriel.nome);

function Carro(cor) {
    this.cor = cor;
    this.descricao = descricao;
}

Carro.prototype.pegaInformacoes = function () {
    return this.descricao + ' e a cor ' + this.cor;
};
let meuCarro = Object.create(Carro.prototype);
meuCarro.cor = 'azul';
// console.log(meuCarro.pegaInformacoes());

const novoCarro = Object.create(Carro, {
    cor: {
        writable: true,
        configurable: true,
        value: 'vermelho',
    },
    descricaoPadrao: {
        writable: false,
        configurable: true,
        value: 'Meu carro',
    },
    descricao: {
        configurable: true,
        get: function () {
            return this.descricaoPadrao.toUpperCase();
        },
        set: function (valor) {
            this.descricaoPadrao = valor.toLowerCase();
        },
    },
});

console.log(novoCarro.descricao);

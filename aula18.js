/**
 * *****
 * File: aula18.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Tuesday, 08 March 2022 17:28:17
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 08 March 2022 17:28:19
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				18. new, constructor e instanceof
 * *****
 */

function Usuario(nome) {
    this.nome = nome;
    this.log = function () {
        console.log(this);
    };
}

const gabriel = new Usuario('Gabriel');
console.log(gabriel);

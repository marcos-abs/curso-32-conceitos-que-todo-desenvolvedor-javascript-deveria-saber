/**
 * *****
 * File: aula28.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 08:45:57
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 08:46:56
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 28. Collections
 * *****
 */

// Primeiro exemplo de coleção (Collection)
// const alfabeto = new Set(['a', 'b', 'c', 'd']);
// alfabeto.add('a');
// console.log('alfabeto: ', alfabeto);
// const resultado = alfabeto.has('a');
// console.log('resultado: ', resultado);
// console.log('alfabeto[0]: ', alfabeto[0]);
// alfabeto.add('e');
// console.log('alfabeto: ', alfabeto);
// alfabeto.delete('a');
// console.log('alfabeto: ', alfabeto);

// let dados = [1, 2, 3, 3, 4, 4, 5];
// const numeros = new Set(dados);
// console.log('numeros: ', numeros);

// dados = Array.from(numeros);
// console.log('dados: ', dados);

// Segundo exemplo de coleção (Collection)
const dados = new Map([
    ['nome', 'gabriel'],
    ['idade', 28],
]);
console.log('dados: ', dados);
dados.set('estado', 'São Paulo');
console.log('dados: ', dados);
const resultado = dados.get('nome');
console.log('resultado: ', resultado);
dados.forEach((dado, chave) => console.log(dado, chave));

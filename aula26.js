/**
 * *****
 * File: aula26.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Saturday, 12 March 2022 18:48:56
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 08:09:18
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 26. High Order Functions
 * *****
 */

// document.getElementById('logo-default').addEventListener('click', function () {
//     alert('clicou no logo!');
// });
function mostrarAlerta() {
    alert('clicou no logo2!');
}

document
    .getElementById('logo-default')
    .addEventListener('click', mostrarAlerta);

function maiusculo(texto) {
    return texto.toUpperCase();
}

const resultado = maiusculo('gabriel');
console.log('resultado: ', resultado);

function chamaAlerta() {
    return mostrarAlerta();
}

chamaAlerta()();

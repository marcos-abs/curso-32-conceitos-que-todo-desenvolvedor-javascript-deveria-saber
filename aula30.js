/**
 * *****
 * File: aula30.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 11:52:20
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 11:52:22
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 30. Promises
 * *****
 */
// const mamaeLembrou = false;
const mamaeLembrou = true;

const passarParaComprarBrinquedo = new Promise((resolve, reject) => {
    if (mamaeLembrou) {
        resolve(true);
    } else {
        reject(false);
    }
});

// passarParaComprarBrinquedo
//     .then((lembrou) => {
//         console.log('lembrou!');
//     })
//     .catch((naoLembrou) => {
//         console.log('não lembrou.');
//     });
console.log('indo para a loja');
const sairParaBrincar = (resultado) => {
    return new Promise((resolve) => {
        if (resultado) {
            resolve('vou sair para brincar.');
        } else {
            resolve('não sairei para brincar');
        }
    });
};

passarParaComprarBrinquedo
    .then(sairParaBrincar)
    .catch(sairParaBrincar)
    .then((resultado) => console.log(resultado));

# dicas

## Tips and Tricks

### for Debugging JavaScript

#### James Q Quick

##### fonte: <https://www.youtube.com/watch?v=_QtUGdaCb1c>

###### console.trace()

Example:
![code](2022-03-10-17-59-13.png)

Result:
![result](2022-03-10-17-59-56.png)

###### console.assert()

```js
console.asset(password !== undefined, "this shoudn't be undefined"); // Isso não deve ser indefinido
```

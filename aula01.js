/**
 * *****
 * File: aula01.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Wednesday, 02 March 2022 10:30:12
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 March 2022 10:44:34
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Primeira aula - FIFO
 * *****
 */

function funcao1() {
    funcao2();
    console.log('Executou função 1');
}

function funcao2() {
    funcao3();
    console.log('Executou função 2');
}

function funcao3() {
    console.log('Executou função 3');
}

funcao1();

/**
 * *****
 * File: aula15.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Tuesday, 08 March 2022 15:15:30
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 08 March 2022 15:15:31
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				15. Factories
 * *****
 */

const Mamifero = function (nome, som) {
    return { nome, som };
};

const cachorro = Mamifero('cachorro', 'auau');
console.log('cachorro: ', cachorro);

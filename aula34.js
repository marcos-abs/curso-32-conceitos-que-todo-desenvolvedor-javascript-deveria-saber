/**
 * *****
 * File: aula34.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Friday, 18 March 2022 10:21:13
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 18 March 2022 10:34:34
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 34. Expensive Operation e Big O Notation
 * *****
 */

/** Big "O" (1) */
const dados = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
// const numero = dados.pop();
// console.log('numero: ', numero);

/** O(n) */
function o_n(entrada, numero) {
    // aka indexOf()
    for (let i = 0, max = entrada.length; i < max; i++) {
        if (entrada[i] === numero) {
            return i;
        }
    }
    return -1;
}
// console.log(o_n(dados, 2));

/** O(n)² */
function o_n_quadrado(entrada) {
    const matriz = [];
    for (let i = 0, max = entrada.length; i < max; i++) {
        matriz[i] = [];
        for (let j = 0, maxj = entrada.length; j < maxj; j++) {
            matriz[i].push(j);
        }
    }
    return matriz;
}

// console.log(o_n_quadrado(dados));

/** O(n log n) */
const entrada = ['q', 'a', 'z', 'w', 's', 'x', 'e', 'd', 'c', 'r'];
function quicksort(entrada) {
    if (entrada.length < 2) {
        return entrada;
    }
    let pivot = entrada[0];
    let esquerda = [];
    let direita = [];
    console.log('pivot: ', pivot);
    for (let i = 1, max = entrada.length; i < max; i++) {
        if (entrada[i] < pivot) {
            esquerda.push(entrada[i]);
        } else {
            direita.push(entrada[i]);
        }
    }
    console.log('esquerda: ', esquerda);
    console.log('direita: ', direita);
    return [...quicksort(esquerda), pivot, ...quicksort(direita)];
}

console.log(quicksort(entrada));

/**
 * *****
 * File: aula11.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Monday, 07 March 2022 11:32:43
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 March 2022 11:32:45
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				11. Fila de eventos e Pilha de eventos
 * *****
 */

function loopEventos() {
    console.log('a');
    for (let i = 0; i < 4; i++) {
        console.log(`b - ${i}`);
    }
    console.log('c');
    setTimeout(() => {
        console.log('d');
    }, 0);
    console.log('e');
}

loopEventos();

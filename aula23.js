/**
 * *****
 * File: aula23.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Friday, 11 March 2022 18:28:12
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 11 March 2022 19:07:17
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				23. Pure functions e side effects
 * *****
 */

// const soma = (x, y) => x + y; // função pura.
// let nome = '';
// const alteraNome = (novoNome) => { // função impura, por ter resultados que afetam o ambiente (??)
//     nome = novoNome;
// };
const usuario = { nome: 'Gabriel', pontos: 0 };
// const alteraUsuario = (usuario) => { // outro exemplo de função impura, por alterar mais de um elemento.
//     usuario.nome = usuario.nome.toUpperCase();
//     usuario.pontos += 1;
//     return usuario;
// };
const nomeMaiusculo = (nome) => nome.toUpperCase(); // função pura.
const incrementaPontos = (pontos) => (pontos += 1); // função pura.

usuario.nome = nomeMaiusculo(usuario.nome);
usuario.pontos = incrementaPontos(usuario.pontos);
console.log('usuario: ', usuario);

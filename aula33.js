/**
 * *****
 * File: aula33.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Friday, 18 March 2022 08:25:29
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 18 March 2022 08:25:31
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 33. Data Structures: Linked list e Tree
 * *****
 */

// Linked List Example

// class Node {
//     constructor(value, next, prev) {
//         this.value = value;
//         this.next = next;
//         this.prev = prev;
//     }
// }

// class LinkedList {
//     constructor() {
//         this.head = null;
//         this.tail = null;
//     }

//     addToHead(value) {
//         const node = new Node(value, null, this.head);
//         if (this.head) this.head.next = node;
//         else this.tail = node;
//         this.head = node;
//     }

//     addToTail(value) {
//         const node = new Node(value, this.tail, null);
//         if (this.tail) this.tail.prev = node;
//         else this.head = node;
//         this.tail = node;
//     }

//     removeHead() {
//         if (!this.head) return null;
//         const value = this.head.value;
//         this.head = this.head.prev;
//         if (this.head) this.head.next = null;
//         else this.tail = null;
//         return value;
//     }

//     removeTail() {
//         if (!this.tail) return null;
//         const value = this.tail.value;
//         this.tail = this.tail.next;
//         if (this.tail) this.tail.prev = null;
//         else this.head = null;
//         return value;
//     }

//     search(value) {
//         let current = this.head;
//         while (current) {
//             if (current.value === value) return value;
//             current = current.prev;
//         }
//         return null;
//     }

//     indexOf(value) {
//         const indexes = [];
//         let current = this.tail;
//         let index = 0;
//         while (current) {
//             if (current.value === value) indexes.push(index);
//             current = current.next;
//             index++;
//         }
//         return indexes;
//     }
// }

// mocha.setup('bdd');
// const { assert } = chai;

// describe('Linked List', () => {
//     it('Should add to head', () => {
//         const list = new LinkedList();
//         list.addToHead(1);
//         list.addToHead(2);
//         list.addToHead(3);
//         assert.equal(list.tail.prev, null);
//         assert.equal(list.tail.value, 1);
//         assert.equal(list.tail.next.value, 2);
//         assert.equal(list.head.prev.value, 2);
//         assert.equal(list.head.value, 3);
//         assert.equal(list.head.next, null);
//         assert.equal(list.head.prev.prev.value, 1);
//         assert.equal(list.tail.next.next.value, 3);
//     });

//     it('Should add to tail', () => {
//         const list = new LinkedList();
//         list.addToTail(1);
//         list.addToTail(2);
//         list.addToTail(3);
//         assert.equal(list.tail.prev, null);
//         assert.equal(list.tail.value, 3);
//         assert.equal(list.tail.next.value, 2);
//         assert.equal(list.head.prev.value, 2);
//         assert.equal(list.head.value, 1);
//         assert.equal(list.head.next, null);
//         assert.equal(list.head.prev.prev.value, 3);
//         assert.equal(list.tail.next.next.value, 1);
//     });

//     it('Should remove head', () => {
//         const list = new LinkedList();
//         list.addToHead(1);
//         list.addToHead(2);
//         list.addToHead(3);
//         assert.equal(list.removeHead(), 3);
//         assert.equal(list.head.value, 2);
//         assert.equal(list.tail.value, 1);
//         assert.equal(list.tail.next.value, 2);
//         assert.equal(list.head.prev.value, 1);
//         assert.equal(list.head.next, null);
//         assert.equal(list.removeHead(), 2);
//         assert.equal(list.head.value, 1);
//         assert.equal(list.tail.value, 1);
//         assert.equal(list.tail.next, null);
//         assert.equal(list.head.prev, null);
//         assert.equal(list.head.next, null);
//         assert.equal(list.removeHead(), 1);
//         assert.equal(list.head, null);
//         assert.equal(list.tail, null);
//     });

//     it('Should remove tail', () => {
//         const list = new LinkedList();
//         list.addToTail(1);
//         list.addToTail(2);
//         list.addToTail(3);
//         assert.equal(list.removeTail(), 3);
//         assert.equal(list.head.value, 1);
//         assert.equal(list.tail.value, 2);
//         assert.equal(list.tail.next.value, 1);
//         assert.equal(list.head.prev.value, 2);
//         assert.equal(list.tail.prev, null);
//         assert.equal(list.removeTail(), 2);
//         assert.equal(list.head.value, 1);
//         assert.equal(list.tail.value, 1);
//         assert.equal(list.tail.next, null);
//         assert.equal(list.head.prev, null);
//         assert.equal(list.tail.prev, null);
//         assert.equal(list.removeTail(), 1);
//         assert.equal(list.head, null);
//         assert.equal(list.tail, null);
//     });

//     it('Should search for value', () => {
//         const list = new LinkedList();
//         list.addToHead(1);
//         list.addToHead(2);
//         list.addToHead(3);
//         assert.equal(list.search(1), 1);
//         assert.equal(list.search(2), 2);
//         assert.equal(list.search(3), 3);
//         assert.equal(list.search(4), null);
//     });

//     it('Should search for indexes of value', () => {
//         const list = new LinkedList();
//         list.addToTail(1);
//         list.addToTail(2);
//         list.addToTail(3);
//         list.addToTail(3);
//         list.addToTail(1);
//         assert.deepEqual(list.indexOf(1), [0, 4]);
//         assert.deepEqual(list.indexOf(2), [3]);
//         assert.deepEqual(list.indexOf(3), [1, 2]);
//         assert.deepEqual(list.indexOf(4), []);
//     });
// });

// mocha.run();

/** Exemplo de Árvore Binária - Binary Tree */

class Tree {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    insert(value) {
        if (value <= this.value) {
            if (!this.left) this.left = new Tree(value);
            else this.left.insert(value);
        } else {
            if (!this.right) this.right = new Tree(value);
            else this.right.insert(value);
        }
    }

    contains(value) {
        if (value === this.value) return true;
        if (value < this.value) {
            if (!this.left) return false;
            else return this.left.contains(value);
        } else {
            if (!this.right) return false;
            else return this.right.contains(value);
        }
    }

    depthFirstTraverse(order, callback) {
        // jshint -W030
        order === 'pre' && callback(this.value);
        this.left && this.left.depthFirstTraverse(order, callback);
        order === 'in' && callback(this.value);
        this.right && this.right.depthFirstTraverse(order, callback);
        order === 'post' && callback(this.value);
        // jshint +W030
    }

    breadthFirstTraverse(callback) {
        // jshint -W030
        const queue = [this];
        while (queue.length) {
            const root = queue.shift();
            callback(root.value);
            root.left && queue.push(root.left);
            root.right && queue.push(root.right);
        }
        // jshint +W030
    }
    getMinValue() {
        if (this.left) return this.left.getMinValue();
        return this.value;
    }

    getMaxValue() {
        if (this.right) return this.right.getMaxValue();
        return this.value;
    }
}

mocha.setup('bdd');
const { assert } = chai;

const tree = new Tree(5);
for (const value of [3, 6, 1, 7, 8, 4, 10, 2, 9]) tree.insert(value);

/*
  5
 3 6
1 4 7
 2   8
		  10
		 9	
*/

describe('Binary Search Tree', () => {
    it('Should implement insert', () => {
        assert.equal(tree.value, 5);
        assert.equal(tree.left.value, 3);
        assert.equal(tree.right.value, 6);
        assert.equal(tree.left.left.value, 1);
        assert.equal(tree.right.right.value, 7);
        assert.equal(tree.right.right.right.value, 8);
        assert.equal(tree.left.right.value, 4);
        assert.equal(tree.right.right.right.right.value, 10);
        assert.equal(tree.left.left.right.value, 2);
        assert.equal(tree.right.right.right.right.left.value, 9);
    });

    it('Should implement contains', () => {
        assert.equal(tree.contains(2), true);
        assert.equal(tree.contains(9), true);
        assert.equal(tree.contains(0), false);
        assert.equal(tree.contains(11), false);
    });

    it('Should implement depthFirstTraverse', () => {
        const _pre = [];
        const _in = [];
        const _post = [];
        tree.depthFirstTraverse('pre', (value) => _pre.push(value));
        tree.depthFirstTraverse('in', (value) => _in.push(value));
        tree.depthFirstTraverse('post', (value) => _post.push(value));
        assert.deepEqual(_pre, [5, 3, 1, 2, 4, 6, 7, 8, 10, 9]);
        assert.deepEqual(_in, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        assert.deepEqual(_post, [2, 1, 4, 3, 9, 10, 8, 7, 6, 5]);
    });

    it('Should implement breadthDepthTraverse', () => {
        const result = [];
        tree.breadthFirstTraverse((value) => result.push(value));
        assert.deepEqual(result, [5, 3, 6, 1, 4, 7, 2, 8, 10, 9]);
    });

    it('Should implement getMinValue', () => {
        assert.equal(tree.getMinValue(), 1);
    });

    it('Should implement getMaxValue', () => {
        assert.equal(tree.getMaxValue(), 10);
    });
});

mocha.run();

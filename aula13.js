/**
 * *****
 * File: aula13.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Monday, 07 March 2022 12:47:57
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 March 2022 12:47:59
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				13. Operadores Bitwise
 * *****
 */

// // conversão de um numero decimal(113) para binário(1110001)
// console.log(Number(113).toString(2)); // resultado: "1110001"

// // conversão de um numero binário(1110001) para decimal(113)
// console.log(parseInt('1110001', 2)); // resultado: "113"

// // Operação "OR" em binário (esta operação "soma" os "bits" ligados):
// // 00000001 or 00000010 = 00000011
// console.log(1 | 2); // resultado "3"
// console.log(parseInt(11, 2)); // resultado "3"

// Operação "AND" em binário (esta operação "soma" os "bits" ligados na mesma posição):
// 00000011 (3) and 00000010 (2) = 00000010 (2 em decimal)
console.log(3 & 2); // resultado "2"
console.log(parseInt(10, 2)); // resultado "2"

/**
 * *****
 * File: aula21.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Wednesday, 09 March 2022 10:46:06
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 09 March 2022 10:59:21
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 *              21. Object.assign
 * *****
 */

const alvo = { a: 1, b: 2, c: 3 };
const dados = { b: 5, c: 4 };
// const resultado = Object.assign(alvo, dados); // resultado:  { a: 1, b: 5, c: 4 }
// console.log('resultado: ', resultado);

// resultado.a = 'a';
// console.log('resultado: ', resultado);
// console.log('alvo: ', alvo);

const obj1 = { a: 1, b: 2, c: 3 };
const obj2 = { d: 4, e: 5, f: 6 };
const resultado = Object.assign({}, obj1, obj2); // resultado: { a: 1, b: 2, c: 3, d: 4, e: 5, f: 6 }
// console.log('resultado: ', resultado);
console.table(resultado);

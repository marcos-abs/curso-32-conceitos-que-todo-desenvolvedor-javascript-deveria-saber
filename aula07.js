/**
 * *****
 * File: aula07.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Friday, 04 March 2022 14:35:22
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 04 March 2022 14:35:24
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				7. Escopo global, de função, do bloco e léxico
 * *****
 */

// var, let e const

// LÉXICO

// var nome = 'Gabriel';
// function teste() {
//     function teste2() {
//         var variavel = 'meu nome';
//     }
// }

// teste();

// GLOBAL

// var a = 0;

// function alterar() {
//     a = 10;
// }
// console.log(`antes a=${a}`);
// alterar();
// console.log(`depois a=${a}`);

// FUNÇÃO
// é igual ao escopo léxico, o que é criado dentro da função não fica disponível fora dela.

// BLOCO
// function bloco() {
//     var teste;
//     if (true) {
//         teste = 'teste';
//         // let teste2 = 'teste2'; // ReferenceError: teste2 is not defined
//         var teste2 = 'teste2'; // it works!
//     }
//     console.log('teste', teste);
//     // jshint -W038
//     console.log('teste2', teste2);
// }
// bloco();

// HOISTING (ELEVAÇÃO)
function bloco() {
    var teste;
    var teste2;
    if (true) {
        teste = 'teste';
        teste2 = 'teste2';
    }
    console.log('teste', teste);
    // jshint -W038
    console.log('teste2', teste2);
}
bloco();

/**
 * *****
 * File: aula04.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Thursday, 03 March 2022 16:27:03
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 03 March 2022 16:27:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				4. Tipos de valores e tipos de referência
 * *****
 */

var x = 10; // mem.0001 = nome é x, e o valor é 10
var y = x; // "y" recebe o valor de "x"
x = 20;
console.log(`1) x=${x} y=${y}`); // 1) x=20 y=10

var x = { valor: 10 };
var y = x; // "y" aponta para o objeto "x"
x.valor = 20;
console.log(`2) x=${x.valor} y=${y.valor}`); // 2) x=20 y=20

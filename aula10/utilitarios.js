/**
 * *****
 * File: utilitarios.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Saturday, 05 March 2022 11:09:37
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 05 March 2022 11:09:40
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				10. Módulos
 * *****
 */

// const notifier = require('node-notifier');
// import * as notifier from 'node-notifier';

const valor = 5;

const helloWorld = function () {
    console.log('Olá Mundo'); // notifier.notify('Olá Mundo!'); // alert('Olá Mundo');
};

const multiplica = function (x) {
    console.log(x * valor); // notifier.notify(x * valor); // alert(x * valor);
};

// export { multiplica, helloWorld };
export default helloWorld;

/**
 * *****
 * File: index.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Saturday, 05 March 2022 11:15:56
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 March 2022 11:21:34
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 10. Módulos
 * *****
 */

// import { helloWorld as olaMundo, multiplica } from './utilitarios.js';
// import * as utilitarios from './utilitarios.js';
import utilitarios from './utilitarios.js';

// utilitarios.multiplica(5);
utilitarios();

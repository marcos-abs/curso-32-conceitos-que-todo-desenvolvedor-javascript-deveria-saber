/**
 * *****
 * File: aula12.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Monday, 07 March 2022 11:39:07
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 March 2022 11:58:19
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				12. SetTimeout, SetInterval, requestAnimationFrame
 * *****
 */
// // Função Anônima comum
// const mostraAlerta = (nome) => {
//     console.log('Set TimeOut - ' + nome);
// };

// // Criação do Threat SetTimeOut
// // setTimeout(mostraAlerta, 3500, 'Gabriel');
// const timeout = setTimeout(mostraAlerta, 3500, 'Gabriel');

// // Cancelamento do Threat SetTimeOut
// setTimeout(() => {
//     clearTimeout(timeout);
// });

// // Criação do SetInterval similar ao SetTimeOut, porém executa continuamente até a sua interrupção com clearInterval()
// const interval = setInterval(() => {
//     console.log('Teste');
// }, 1000);

// // Cancelamento do Threat SetInterval
// setTimeout(() => {
//     clearInterval(interval);
// }, 3000);

// Animation Frame (!!) *** só funciona do navegador ***
let contador = 0;

function animation() {
    contador += 1;
    console.log('contador:', contador);
    loop = requestAnimationFrame(animation);
}

var loop = requestAnimationFrame(animation);

setTimeout(() => {
    cancelAnimationFrame(loop);
}, 5000);

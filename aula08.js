/**
 * *****
 * File: aula08.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Friday, 04 March 2022 15:10:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 04 March 2022 15:10:31
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				8. Expressão e Declaração
 * *****
 */

console.log(1 + 1);
console.log(Math.random() + 5);

function expressao() {
    return 1 + 1;
}
console.log('expressao: ', expressao);

var variavel = 20;
if (true) {
    variavel = 30;
}

function soma(a) {
    return a + 10;
}

console.log(soma(Math.random()));

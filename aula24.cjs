/**
 * *****
 * File: aula24.cjs
 * Project: Curso 32 conceitos Javascript
 * File Created: Saturday, 12 March 2022 12:27:21
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 12 March 2022 12:34:34
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 24. - State Mutation
 * *****
 */
const pedido = { titulo: 'meu pedido', status: false };
const copiaPedido = (pedido) => {
    /*****
     * Forma incorreta, pois permite alterar ambos os conteúdos, original e resultante.
     * veja o exemplo abaixo:
     *         pedido.status = true;
     *         return pedido;
     * Agora veja a forma e o resultado corretos.
     *****/
    const novoPedido = Object.assign({}, pedido);
    novoPedido.status = true;
    return novoPedido;
};
const novoPedido = copiaPedido(pedido);
novoPedido.titulo = 'Segundo Pedido';
console.log('pedido: ', pedido);
console.log('pedido: ', novoPedido);

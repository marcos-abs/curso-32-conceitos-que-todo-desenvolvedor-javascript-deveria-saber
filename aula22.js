/**
 * *****
 * File: aula22.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Friday, 11 March 2022 16:59:19
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 11 March 2022 16:59:21
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 *              22. map, reduce e filter
 * *****
 */

const dados = [
    { nome: 'Caramelo', idade: 3, tipo: 'cachorro' },
    { nome: 'Rex', idade: 6, tipo: 'cachorro' },
    { nome: 'Bolota', idade: 1, tipo: 'gato' },
    { nome: 'Thor', idade: 3, tipo: 'cachorro' },
];
const cachorros = dados.filter((dado, index) => dado.tipo === 'cachorro');
// console.log(cachorros);
const idadeReal = cachorros.map((cachorro) => ({
    nome: cachorro.nome,
    idade: cachorro.idade * 7,
}));
// console.log('idadeReal: ', idadeReal);
// jshint -W138
const totalIdades = idadeReal.reduce((soma = 0, dado) => {
    return soma + dado.idade;
}, 0);
// console.log('totalIdades: ', totalIdades);
const idade = dados
    .filter((dado, index) => dado.tipo === 'cachorro')
    .map((cachorro) => ({
        nome: cachorro.nome,
        idade: cachorro.idade * 7,
    }))
    .reduce((soma = 0, dado) => {
        return soma + dado.idade;
    }, 0);
// jshint +W138
console.log('idade: ', idade);

/**
 * *****
 * File: aula05.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Thursday, 03 March 2022 17:10:16
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 03 March 2022 17:12:57
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 - 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				5. Implícito, Explicito e chamada de métodos
 * *****
 */

// Coerção
// console.log('5' - 5); // 0
// console.log('5' + 5); // "55"
// console.log(true + 1); // 2
// console.log(true + true); // 2
// console.log([] + {}); // "[object Object]"
// console.log([] + []); // ""

// Implícito
// console.log(+'5'); // 5
// console.log(5 + ''); // "5"
// console.log(123 && 'oi'); // "oi"
// console.log(null || true); // true

// Explicito
// console.log(Number('50')); // 50
// console.log(String(50)); // "50"

// "DuckType: Se anda como pato e fala como pato só pode ser pato."

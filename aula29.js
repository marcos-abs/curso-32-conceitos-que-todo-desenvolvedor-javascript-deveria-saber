/**
 * *****
 * File: aula29.js
 * Project: Curso 32 conceitos Javascript
 * Path: /home/desenvolvedor/cursos/curso-32-conceitos-javascript
 * File Created: Monday, 14 March 2022 11:33:53
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 14 March 2022 11:33:57
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 29. Generators
 * *****
 */
// Exemplo de laço sem iterações
// function iteraNumeros(total) {
//     for (let i = 1; i <= total; i++) {
//         console.log('i: ', i);
//     }
// }
// iteraNumeros(5);

/**
 * iteraGenerator - 1o. exemplo
 * @param {int} total
 */
// function* iteraGenerator(total) {
//     for (let i = 1; i <= total; i++) {
//         yield console.log('i: ', i);
//     }
// }

// const totalGenerator = iteraGenerator(5);

// totalGenerator.next();
// totalGenerator.next();
// totalGenerator.next();
// totalGenerator.next();
// totalGenerator.next();
// totalGenerator.next();

/**
 * iteraGenerator - 2o. exemplo
 * @param {int} total
 */
function* iteraGenerator(total) {
    for (let i = 1; i <= total; i++) {
        yield i;
    }
}

const totalGenerator = iteraGenerator(5);
console.log('totalGenerator: ', totalGenerator.next().value);
console.log('totalGenerator: ', totalGenerator.next().value);
console.log('totalGenerator: ', totalGenerator.next().value);
console.log('totalGenerator: ', totalGenerator.next().value);
console.log('totalGenerator: ', totalGenerator.next().value);
console.log('totalGenerator: ', totalGenerator.next().value);

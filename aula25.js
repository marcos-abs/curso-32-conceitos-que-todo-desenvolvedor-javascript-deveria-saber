/**
 * *****
 * File: aula25.js
 * Project: Curso 32 conceitos Javascript
 * File Created: Saturday, 12 March 2022 13:58:27
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Saturday, 12 March 2022 14:09:07
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Aula 25. Closures
 * *****
 */
var nome = 'Gabriel';
function olaPessoa() {
    console.log('Olá ' + nome);
}
nome = 'Barreto';
olaPessoa(); // Resultado: Olá Barreto // por quê? por que ela é um "closure", ou seja, não guarda o valor em memoria durante a sua execução e sim o endereço onde será buscando o resultado a ser apresentado.

// outro exemplo:
function buscaItem() {
    var numeroItem = 150;
    fetch('/minha/url/do/banco-de-dados?item=' + numeroItem, {
        method: 'GET',
    }).then((resultado) => {
        console.log('Busca pelo item ' + numeroItem + 'deu certo!'); // outro closure, pois as callbacks também são funções.
    });
}
